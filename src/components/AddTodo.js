import React from 'react'


// Purpose: send input values to App
const AddTodo = props => {
    return (
        <div>
            <input
                onChange={props.updateField}
                type="text"
                placeholder="To do"
                value={props.currentTodo.text}
            />
            <button onClick={props.addTodo} className="btn btn-success"> Add </button>
        </div>
    )
}

export default AddTodo
