import React, { Component } from 'react'
import '../App.css'

class Todo extends Component {
    handleDelete(){
        // console.log('Todo', this.props.id)
        this.props.deleteTodo(this.props.id)
    }
    render() {
        return (
            <li className="list-group-item" >
                <input type="checkbox" defaultChecked={this.props.isDone} />
                <span>{this.props.text}</span>
                <div className="btn-group">
                    <input type="button" onClick={this.props.editTodo} className="btn btn-info" value="edit"/>
                    <input type="button" onClick={this.handleDelete.bind(this)} className="btn btn-danger" value="x"/>
                </div>
            </li>
        )
    }
}

export default Todo
