import React, { Component } from 'react'
import { Todo } from './'

class TodoList extends Component {
    constructor(props){
        super(props)
        this.state = {
            editing: ''
        }
    }

    handleDeleteTodo(e) {
        this.props.onDelete(e)
        // console.log('TodoList', e)
    }

    render () {
        const todos = this.props.todos.map((todo, index) =>
            <Todo
                key={`todo-${index}`}
                id={index}
                isDone={todo.isDone}
                text={todo.text}
                deleteTodo={this.handleDeleteTodo.bind(this)}
            />
        )
        return (
            <ul className="list-group">
                {todos}
            </ul>
        )
    }
}


export default TodoList
