import React, { Component } from 'react';
import './App.css';
import '../public/styles/bootstrap.min.css';
import { AddTodo, TodoList } from './components'

class App extends Component {
  constructor(){
    super()

    this.state = {
        currentTodo: {
            id: '',
            text: '',
            isDone: false
        },
        todos: [
            {
                id: 1,
                text: 'Go shopping',
                isDone: false
            },
            {
                id: 2,
                text: 'Clean house',
                isDone: true
            },
            {
                id: 3,
                text: 'Learn React',
                isDone: false
            },
            {
                id: 4,
                text: 'Make Green Smoothie',
                isDone: false
            }
        ]
    }
    }

    addTodo() {
        // only add item if input field is not empty
        if(this.state.currentTodo.text.trim() !== '') {
            // make a copy of current this.state.todos
            let updatedTodos = Object.assign([], this.state.todos)
            // set copied todos to currentTodo
            updatedTodos.push(this.state.currentTodo)
            // push currentTodo to mutated copy of state
            this.setState({ todos: updatedTodos })
            return
        }
        alert('enter something!')
    }

    // should send new object to list of todos
    // this object will come from the input field from <AddTodo />
    updateTodo(event) {
        let updatedCurrentTodo = Object.assign({}, this.state.currentTodo)
        updatedCurrentTodo['text'] = event.target.value
        updatedCurrentTodo['id'] = this.state.todos.length + 1
        this.setState( { currentTodo: updatedCurrentTodo } )
    }

    handleTodoDelete(todoId) {
        // remove item from array
        // maybe move this to utility function later
        function removeTodo(todos, index) {
            return [
                ...todos.slice(0, index),
                ...todos.slice(index + 1)
            ]
        }

        this.setState({todos: removeTodo(this.state.todos, todoId)});
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-6 center">
                        <AddTodo
                            currentTodo={this.state.currentTodo}
                            updateField={this.updateTodo.bind(this)}
                            addTodo={this.addTodo.bind(this)}
                        />
                        <TodoList todos={this.state.todos} onDelete={this.handleTodoDelete.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
